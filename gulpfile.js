const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');

const css = (done) => {
  gulp.src('scss/bootstrap.scss')
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe(gulp.dest('css'));
  done();
};

gulp.task('css', css);

gulp.task('watch', () => {
  gulp.watch('scss/**/*.scss', {usePolling: true}, css);
});
